import 'package:flutter/material.dart';

import '../constants.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key key,
    this.color,
    @required this.label,
    @required this.onPressed,
  }) : super(key: key);

  final Color color;
  final String label;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: color ?? Colors.blueGrey, // background
          onPrimary: Colors.white, // foreground
        ),
        child: Padding(
          padding: EdgeInsets.all(kPaddingNormal),
          child: Text(
            label,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
