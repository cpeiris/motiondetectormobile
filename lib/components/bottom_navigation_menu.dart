import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:motion_detector/controllers/bottom_navigation_controller.dart';
import 'package:motion_detector/views/home_screen.dart';
import 'package:motion_detector/views/settings_screen.dart';

class BottomNavigationMenu extends StatelessWidget {
  BottomNavigationMenu({Key key}) : super(key: key);

  final BottomNavigationController bottomNavigationController =
      Get.put(BottomNavigationController());

  static List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    SettingsScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => _widgetOptions
            .elementAt(bottomNavigationController.selectedIndex.value),
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
          currentIndex: bottomNavigationController.selectedIndex.value,
          selectedItemColor: Colors.blueAccent,
          unselectedItemColor: Colors.grey,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          onTap: bottomNavigationController.selectedIndex,
        ),
      ),
    );
  }
}
