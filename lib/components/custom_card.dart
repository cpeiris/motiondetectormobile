import 'package:flutter/material.dart';

import '../constants.dart';

class CustomCard extends StatelessWidget {
  const CustomCard({
    Key key,
    @required this.color,
    @required this.child,
  }) : super(key: key);

  final Widget child;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(kCornerRadius),
      ),
      elevation: kElevationGeneral,
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(kCornerRadius),
            gradient: LinearGradient(
              colors: [
                color == Colors.white ? Colors.white : Colors.blueAccent,
                color,
              ],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 0.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp,
            ),
          ),
          child: child),
    );
  }
}
