import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:motion_detector/components/bottom_navigation_menu.dart';
import 'package:motion_detector/views/login_screen.dart';

final box = GetStorage();

List<GetPage> buildRoutes = [
  GetPage(
    name: '/',
    page: () => (box.hasData('isLoggedIn') && box.read('isLoggedIn'))
        ? BottomNavigationMenu()
        : LoginScreen(),
  ),
  GetPage(
    name: '/home',
    page: () => BottomNavigationMenu(),
  ),
];
