import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:motion_detector/models/locationDevices.dart';
import 'package:motion_detector/models/locations.dart';
import 'package:motion_detector/services/remote_services.dart';
import 'package:motion_detector/views/login_screen.dart';

import '../constants.dart';

class SettingsController extends GetxController {
  var isLoading = true.obs;
  var selectedLocation = "".obs;
  var selectedDevice = "".obs;
  var selectedCurrentLocation = "".obs;
  var selectedNewLocation = "".obs;

  var alarmPeriod = 1.obs;

  List<Locations> listLocations = <Locations>[];
  List<LocationDevices> listDevices = <LocationDevices>[];

  List<String> locationNames = <String>[].obs;
  List<String> locationDevices = <String>[].obs;

  final box = GetStorage();

  @override
  void onInit() {
    if (listLocations.length == 0) fetchLocations();
    super.onInit();
  }

  void setCurrentLocation(String locationName) {
    selectedCurrentLocation.value = locationName;

    String locationId;

    for (Locations location in listLocations) {
      if (location.locationName == selectedCurrentLocation.value)
        locationId = location.locationId.toString();
    }

    fetchLocationDevices(locationId);
  }

  void fetchLocations() async {
    try {
      var response = await RemoteServices.fetchLocations();
      if (response != null) {
        listLocations = response;

        for (Locations location in listLocations) {
          locationNames.add(location.locationName);
        }

        if (locationNames.length > 0) {
          alarmPeriod(listLocations[0].triggerPeriod);

          String defaultLocation = locationNames[0];
          selectedLocation(defaultLocation);
          selectedCurrentLocation(defaultLocation);
          selectedNewLocation(defaultLocation);

          fetchLocationDevices(listLocations[0].locationId.toString());

          update();
        }
        print('fetchLocations: ${jsonEncode(locationNames)}');
      }
    } catch (e) {
      print(e.toString());
    }
  }

  void fetchLocationDevices(String locationId) async {
    try {
      var response = await RemoteServices.fetchDevicesByLocation(locationId);
      if (response != null) {
        listDevices = response;

        locationDevices.clear();

        for (LocationDevices device in listDevices) {
          locationDevices.add(device.deviceName);
        }

        if (locationDevices.length > 0) {
          selectedDevice(locationDevices[0]);
          update();
        }

        print('fetchLocationDevices: ${jsonEncode(locationDevices)}');
      }
    } catch (e) {
      print(e.toString());
    }
  }

  void postDeviceSettings(int whichSettings) async {
    try {
      if (box.hasData('user_role_id') && box.read('user_role_id') != 1) {
        Fluttertoast.showToast(
            msg: "You have no permission to change the settings.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: kTextSizeToast);
        return;
      }

      int deviceId, locationId;

      if (whichSettings == 4 && listDevices.length > 0) {
        for (LocationDevices device in listDevices) {
          if (device.deviceName == selectedDevice.value)
            deviceId = device.deviceId;
        }
      }

      if (listLocations.length > 0) {
        for (Locations location in listLocations) {
          if (whichSettings == 1 &&
              location.locationName == selectedLocation.value)
            locationId = location.locationId;
          else if (whichSettings == 4 &&
              location.locationName == selectedNewLocation.value)
            locationId = location.locationId;
        }
      }

      var response = await RemoteServices.postSettings(
          whichSettings: whichSettings,
          alarmPeriod: alarmPeriod.value.toString(),
          locationId: locationId,
          deviceName: selectedDevice.value,
          deviceId: deviceId);

      String message;
      Color messageColor;

      if (response) {
        message = "Operation successful!";
        messageColor = Colors.green;
      } else {
        message = "Operation failed!";
        messageColor = Colors.red;
      }

      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: messageColor,
          textColor: Colors.white,
          fontSize: kTextSizeToast);
    } catch (e) {
      print(e.toString());
    }
  }

  void logOut() {
    box.write("isLoggedIn", false);
    Get.offAll(() => LoginScreen());
  }
}
