
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:motion_detector/components/bottom_navigation_menu.dart';
import 'package:motion_detector/helpers/utils.dart' as Utils;
import 'package:motion_detector/models/login.dart';
import 'package:motion_detector/services/remote_services.dart';

class LoginController extends GetxController {
  String username = "", password = "";
  bool isSignInSuccess;
  var isLoading = true.obs;

  final box = GetStorage();

  void login() async {
    try {
      if (username.isEmpty || password.isEmpty) {
        Utils.showToast(message: "Invalid Input", backgroundColor: Colors.red);
        return;
      }

      isLoading(true);

      Login loginResponse = await RemoteServices.login(username, password);

      print('Login Response: $loginResponse');

      if (loginResponse != null) {
        isSignInSuccess = true;
        box.write('isLoggedIn', true);
        box.write('user_id', loginResponse.userId);
        box.write('user_name', loginResponse.name);
        box.write('user_role_id', loginResponse.userRoleId);
        Get.off(() => BottomNavigationMenu());
      } else {
        isSignInSuccess = false;
        Utils.showToast(
            message: "Invalid credentials", backgroundColor: Colors.red);
      }
    } catch (e) {
      print(e.toString());
    } finally {
      isLoading(false);
    }
  }

  // void configureAmplify() async {
  //   AmplifyAnalyticsPinpoint analyticsPlugin = AmplifyAnalyticsPinpoint();
  //   AmplifyAuthCognito authPlugin = AmplifyAuthCognito();
  //   Amplify.addPlugins([authPlugin, analyticsPlugin]);
  //
  //   try {
  //     await Amplify.configure(amplifyconfig);
  //   } on AmplifyAlreadyConfiguredException {
  //     print(
  //         "Tried to reconfigure Amplify; this can occur when your app restarts on Android.");
  //   }
  // }
  //
  // Future<bool> signIn() async {
  //   print(
  //       "LoginController | username: " + username + ", password: " + password);
  //
  //   try {
  //     SignInResult res = await Amplify.Auth.signIn(
  //       username: username,
  //       password: password,
  //     );
  //     isSignInSuccess = res.isSignedIn;
  //     print("LoginController | " + isSignInSuccess.toString());
  //     return true;
  //   } on AuthException catch (e) {
  //     print("LoginController | " + e.message);
  //     return false;
  //   }
  // }
}
