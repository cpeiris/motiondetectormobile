import 'dart:convert';
import 'dart:typed_data';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/state_manager.dart';
import 'package:intl/intl.dart';
import 'package:motion_detector/helpers/utils.dart' as Utils;
import 'package:motion_detector/models/devices.dart';
import 'package:motion_detector/models/status.dart';
import 'package:motion_detector/services/remote_services.dart';
import 'package:package_info/package_info.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

const MethodChannel platform =
    MethodChannel('dexterx.dev/flutter_local_notifications_example');

class HomeController extends GetxController {
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  RxList listDevices = <Devices>[].obs;
  RxList listStatus = <int>[].obs;
  String clickedDevice;

  RefreshController refreshController = RefreshController(initialRefresh: true);
  var isLoading = true.obs;
  PackageInfo packageInfo;

  RxString version = "".obs;

  @override
  void onInit() async {
    await FirebaseMessaging.instance.subscribeToTopic('esoft');
    notificationPermission();
    initMessaging();

    packageInfo = await PackageInfo.fromPlatform();
    version(packageInfo.version);

    super.onInit();
  }

  String getCurrentTime() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    return formattedDate;
  }

  String convertTimeStamp(int deviceId, String timeStamp) {
    if (timeStamp == null) return "No information";

    DateTime receivedTime = DateTime.fromMillisecondsSinceEpoch(
        (int.parse(timeStamp) * 1000) - 19800);

    String formattedDate =
        DateFormat('yyyy-MM-dd HH:mm:ss').format(receivedTime);

    return formattedDate;
  }

  int timeDifference(String timeStamp) {
    try {
      DateTime now = DateTime.now();
      DateTime receivedTime = DateTime.fromMillisecondsSinceEpoch(
          (int.parse(timeStamp) * 1000) - 19800);

      int differenceInMinutes = now.difference(receivedTime).inMinutes;
      return differenceInMinutes;
    } catch (e) {
      print(e);
      return null;
    }
    // print('Time Difference: $deviceId - $differenceInMinutes');
  }

  String timeDifferenceMessage(int difference) {
    print('difference: $difference');

    if (difference < 60)
      return '$difference minutes';
    else if (difference > 60 && difference < 120)
      return '1 hour';
    else if (difference > 120 && difference < 1440)
      return '${difference ~/ 60} hours';
    else if (difference > 1440 && difference < 2880)
      return '1 day';
    else
      return '${difference ~/ 1440} days';
  }

  void onRefresh() {
    print('calling on refresh');
    fetchDevices();
  }

  void fetchDevices() async {
    try {
      isLoading(true);

      var response = await RemoteServices.fetchDevices();

      if (response != null) {
        listDevices.assignAll(response);

        String devices = "";

        for (Devices device in listDevices) {
          devices.isEmpty
              ? devices = device.deviceId.toString()
              : devices = devices + "," + device.deviceId.toString();
        }

        if (devices.isNotEmpty) {
          List<Status> deviceResponse =
              await RemoteServices.fetchDeviceStatus(devices);

          Map<String, dynamic> statusData;

          print('device count: ${listDevices.length}');
          print('status count: ${deviceResponse.length}');

          for (Status status in deviceResponse) {
            statusData = jsonDecode(status.data);

            for (Devices device in listDevices) {
              try {
                if (device.deviceId == status.deviceId) {
                  device.status = statusData['status'];

                  // statusData['status'] == "ON"
                  //     ? device.isON = true
                  //     : device.isON = false;

                  if (statusData['status'] == null)
                    return;
                  else if (statusData['status'] == "ON") {
                    device.isON = true;
                    device.status = "ON";
                  } else {
                    device.isON = false;
                    device.status = "OFF";
                  }

                  device.lastUpdated = statusData['time_stamp'];

                  print(
                      'device ${jsonEncode(device.deviceId)} : ${jsonEncode(device)}');
                }

                /*else {
                  device.isON = false;
                }*/
              } catch (e) {
                device.status = null;
              }
            }
          }

          // log('updated list: ${jsonEncode(listDevices)}');
        }
      }
    } finally {
      isLoading(false);
      refreshController.refreshCompleted();

      update();
    }
  }

  void sendDeviceCommand(
      String command, String deviceId, String deviceName) async {
    String toast =
        'Sending device command $command for the device $deviceName...';

    Utils.showToast(message: toast, backgroundColor: Colors.blueAccent);

    try {
      isLoading(true);
      clickedDevice = deviceId;
      update();

      bool commandResponse =
          await RemoteServices.postDeviceCommand(command, deviceId);

      if (commandResponse) {
        List<Status> deviceResponse;

        Future.delayed(Duration(milliseconds: 2500), () async {
          print('calling deviceResponse');
          deviceResponse = await RemoteServices.fetchDeviceStatus(deviceId);

          Map<String, dynamic> statusData;

          print("deviceResponse: ${jsonEncode(deviceResponse)}");

          for (Status status in deviceResponse) {
            statusData = jsonDecode(status.data);

            for (Devices device in listDevices) {
              try {
                if (device.deviceId == status.deviceId) {
                  device.status = statusData['status'];
                  device.isON = device.status == "ON" ? true : false;
                }
              } catch (e) {
                device.status = null;
              }
            }

            print("device status: ${jsonEncode(listDevices)}");
          }

          isLoading(false);
          update();
        });
      }
    } catch (e) {
      print("sendDeviceCommand: ${jsonEncode(e)}");
    }
  }

  bool deviceStatus(int index) {
    print('${listDevices[index].deviceName} : ${listDevices[index].status}');

    if (listDevices[index].isON)
      return true;
    else
      return false;
  }

  void powerButton(int index) {
    clickedDevice = listDevices[index].deviceId.toString();
    String command = listDevices[index].isON ? "OFF" : "ON";

    // print('Device ID: ${listDevices[index].deviceId.toString()}');

    sendDeviceCommand(command, listDevices[index].deviceId.toString(),
        listDevices[index].deviceName);
  }

  /*Widget customSwitch(int index, bool status) {
    print('value $status');

    return listDevices[index].status == null
        ? Container()
        : (isLoading() &&
                clickedDevice == listDevices[index].deviceId.toString()
            ? CircularProgressIndicator(
                backgroundColor: Colors.white,
              )
            : CustomSwitch(
                onChanged: (_) {
                  listDevices[index].status == "ON"
                      ? listDevices[index].status = "OFF"
                      : listDevices[index].status = "ON";

                  listDevices[index].isON
                      ? listDevices[index].status = false
                      : listDevices[index].status = true;

                  sendDeviceCommand(
                      listDevices[index].status,
                      listDevices[index].deviceId.toString(),
                      listDevices[index].deviceName);
                },
                value: status,
                activeColor: Colors.red,
              ));
  }*/

  void initMessaging() {
    var androidInit = AndroidInitializationSettings('ic_launcher');

    var iosInit = IOSInitializationSettings();

    var initSetting =
        InitializationSettings(android: androidInit, iOS: iosInit);

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.initialize(initSetting);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      showNotification(message.data['title'], message.data['body']);
    });

    messaging.getToken().then((token) async {
      print('firebase token: ' + token);
      RemoteServices.postFirebaseToken(firebaseToken: token);
    });
  }

  void showNotification(String title, String message) async {
    final Int64List vibrationPattern = Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    var androidDetails = AndroidNotificationDetails(
        '1', 'channelName', 'channel Description',
        importance: Importance.max,
        priority: Priority.high,
        sound: RawResourceAndroidNotificationSound('alarm'),
        vibrationPattern: vibrationPattern,
        enableLights: true,
        color: const Color.fromARGB(255, 255, 0, 0),
        ledColor: const Color.fromARGB(255, 255, 0, 0),
        ledOnMs: 1000,
        ledOffMs: 500);

    var iosDetails = IOSNotificationDetails();

    var generalNotificationDetails =
        NotificationDetails(android: androidDetails, iOS: iosDetails);

    await flutterLocalNotificationsPlugin.show(
        0, title, message, generalNotificationDetails,
        payload: 'Notification');
  }

  void notificationPermission() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }
}
