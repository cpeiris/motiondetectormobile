import 'package:flutter/material.dart';

// COLORS
const kPrimaryColor = Color(0xff19469D);
const kDarkGreyColor = Color(0xff3A3A3B);
const kGreyColor = Color(0xffAFAFAF);
const kLightGreyColor = Color(0xfffafaff);
const kBarGreenColor = Color(0xff01701b);
const kBarPurpleColor = Color(0xff4f147d);

const kCardColor = Colors.blueAccent;

// TEXT STYLES
const kTextStyleTitle = TextStyle(fontSize: 24, fontWeight: FontWeight.bold);

// settings card
const kTextStyleCardTitle =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16);

// device card
const kTextStyleCardDeviceName =
    TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 20);

const kTextStyleDeviceStatusTitle =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12);

const kTextStyleDeviceStatus =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30);

const kTextStyleDeviceStatusNoInfo =
    TextStyle(color: Colors.white, fontSize: 11);

const kTextStyleDeviceTimeDifference = TextStyle(
  color: Colors.white,
  fontSize: 14,
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.italic,
);

// DIMENS

// text size
const double kTextSizeToast = 14;

// button size
const double kButtonHeight = 50;

// elevation
const double kElevationGeneral = 2;
const int kDropDownElevation = 12;

// padding
const double kPaddingSmall = 8;
const double kPaddingNormal = 16;
const double kPaddingLarge = 32;

// margin
const double kMarginSmall = 8;
const double kMarginNormal = 16;
const double kMarginLarge = 32;

// radius
const double kCornerRadius = 10;
const int kBorderRadius = 10;

// DECORATIONS
const BoxDecoration kDecorationTopCard = BoxDecoration(
  color: Colors.blueAccent,
  borderRadius: BorderRadius.only(
    topLeft: Radius.circular(kCornerRadius),
    topRight: Radius.circular(kCornerRadius),
  ),
);
