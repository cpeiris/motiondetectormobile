import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:motion_detector/constants.dart';
import 'package:motion_detector/controllers/login_controller.dart';

class LoginScreen extends StatelessWidget {
  final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double safe = MediaQuery.of(context).padding.top;

    height = height - safe;

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: height,
            color: kLightGreyColor,
            padding: EdgeInsets.all(kPaddingNormal),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Center(
                  child: Text(
                    'Motion Device Manager',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: kPrimaryColor),
                  ),
                ),
                SvgPicture.asset(
                  'assets/image_login.svg',
                  height: 200,
                  width: 200,
                ),
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(kPaddingNormal),
                    child: Column(
                      children: [
                        TextFormField(
                          initialValue: loginController.username,
                          onChanged: (value) {
                            loginController.username = value;
                          },
                          decoration:
                              InputDecoration(labelText: 'Enter username'),
                        ),
                        TextFormField(
                          initialValue: loginController.password,
                          onChanged: (value) {
                            loginController.password = value;
                          },
                          obscureText: true,
                          decoration:
                              InputDecoration(labelText: 'Enter password'),
                        ),
                        SizedBox(
                          height: kMarginNormal,
                        ),
                        Container(
                          height: kButtonHeight,
                          width: double.infinity,
                          child: ElevatedButton(
                            onPressed: () => loginController.login(),
                            child: Text("Login"),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
