import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:motion_detector/components/background.dart';
import 'package:motion_detector/components/custom_card.dart';
import 'package:motion_detector/constants.dart';
import 'package:motion_detector/controllers/home_controller.dart';
import 'package:motion_detector/models/devices.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatelessWidget {
  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return Background(
      child: Container(
        padding: EdgeInsets.all(kPaddingNormal),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Text(
                "Motion Detector",
                style: kTextStyleTitle.apply(color: Colors.blueAccent),
              ),
            ),
            Obx(
              () => Center(
                child: Text('version ${homeController.version()}'),
              ),
            ),
            SizedBox(
              height: kMarginNormal,
            ),
            Expanded(
              child: GetBuilder<HomeController>(
                builder: (_) => SmartRefresher(
                  controller: homeController.refreshController,
                  enablePullDown: true,
                  enablePullUp: false,
                  onRefresh: homeController.onRefresh,
                  header: WaterDropHeader(),
                  child: ListView.builder(
                    itemCount: homeController.listDevices.length,
                    itemBuilder: (context, index) {
                      Devices itemDevice = homeController.listDevices[index];
                      return CustomCard(
                        color: itemDevice.isON == null
                            ? Colors.lightBlue
                            : itemDevice.isON
                                ? Colors.red
                                : Colors.lightBlue,
                        child: Padding(
                          padding: EdgeInsets.all(kPaddingNormal),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Text(
                                          itemDevice.deviceName,
                                          style: kTextStyleCardDeviceName,
                                        ),
                                        Text(
                                          itemDevice.locationName,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 24,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        Text(
                                          'Device ID: ${itemDevice.deviceId.toString()}',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                        SizedBox(
                                          height: kMarginNormal,
                                        ),
                                        Text(
                                          "Last Updated:\n${homeController.convertTimeStamp(itemDevice.deviceId, itemDevice.lastUpdated)}",
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text('Last Known Status',
                                          style: kTextStyleDeviceStatusTitle),
                                      Text(
                                        itemDevice.isON == null
                                            ? 'No Information'
                                            : itemDevice.isON
                                                ? 'ON'
                                                : 'OFF',
                                        style: itemDevice.isON == null
                                            ? kTextStyleDeviceStatusNoInfo
                                            : kTextStyleDeviceStatus,
                                      ),
                                      SizedBox(
                                        height: kMarginNormal,
                                      ),
                                      itemDevice.status == null
                                          ? Container()
                                          : homeController.isLoading() &&
                                                  homeController
                                                          .clickedDevice ==
                                                      itemDevice.deviceId
                                                          .toString()
                                              ? Center(
                                                  child:
                                                      CircularProgressIndicator(
                                                    backgroundColor:
                                                        Colors.white,
                                                    valueColor:
                                                        AlwaysStoppedAnimation<
                                                            Color>(
                                                      itemDevice.isON
                                                          ? Colors.red
                                                          : Colors.lightBlue,
                                                    ),
                                                  ),
                                                )
                                              : Container(
                                                  width: 36,
                                                  height: 36,
                                                  child: FloatingActionButton(
                                                    backgroundColor:
                                                        homeController
                                                                .listDevices[
                                                                    index]
                                                                .isON
                                                            ? Colors.red
                                                            : Colors.grey,
                                                    child: Icon(
                                                      Icons.power_settings_new,
                                                      size: 30,
                                                    ),
                                                    onPressed: () =>
                                                        homeController
                                                            .powerButton(index),
                                                  ),
                                                ),
                                    ],
                                  ),
                                ],
                              ),
                              itemDevice.lastUpdated == null ||
                                      itemDevice.isON == false
                                  ? Container()
                                  : SizedBox(
                                      height: kMarginNormal,
                                    ),
                              itemDevice.lastUpdated == null
                                  ? Container()
                                  : itemDevice.isON != null &&
                                          itemDevice.isON &&
                                          homeController.timeDifference(
                                                  itemDevice.lastUpdated) >
                                              60
                                      ? Text(
                                          'Note: No information has received for this device from ${homeController.timeDifferenceMessage(homeController.timeDifference(itemDevice.lastUpdated))}.',
                                          style: kTextStyleDeviceTimeDifference,
                                        )
                                      : Container(),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
