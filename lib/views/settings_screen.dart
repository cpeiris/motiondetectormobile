import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:motion_detector/components/background.dart';
import 'package:motion_detector/components/custom_button.dart';
import 'package:motion_detector/components/custom_card.dart';
import 'package:motion_detector/controllers/settings_controller.dart';

import '../constants.dart';

class SettingsScreen extends StatelessWidget {
  final settingsController = Get.put(SettingsController());

  @override
  Widget build(BuildContext context) {
    return Background(
      child: Container(
        color: kLightGreyColor,
        padding: EdgeInsets.all(kPaddingNormal),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Text(
                "Settings",
                style: kTextStyleTitle.apply(color: Colors.blueAccent),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: kMarginNormal,
                    ),
                    CustomCard(
                      color: Colors.white,
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              decoration: kDecorationTopCard,
                              padding: EdgeInsets.all(kPaddingNormal),
                              child: Center(
                                child: Text(
                                  "Update Alarm Period",
                                  style: kTextStyleCardTitle,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(kPaddingNormal),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text("Select Location"),
                                  Obx(
                                    () => DropdownButton(
                                      key: Key("location"),
                                      elevation: kDropDownElevation,
                                      items: settingsController.locationNames
                                          .map<DropdownMenuItem<String>>(
                                        (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                      onChanged: (value) {
                                        settingsController
                                            .selectedLocation(value);
                                      },
                                      value: settingsController
                                          .selectedLocation.value,
                                      underline: Container(),
                                    ),
                                  ),
                                  SizedBox(
                                    height: kMarginNormal,
                                  ),
                                  Text("Alarm Period (mins)"),
                                  Obx(
                                    () => TextFormField(
                                      initialValue: settingsController
                                          .alarmPeriod.value
                                          .toString(),
                                      onChanged: (value) =>
                                          settingsController.alarmPeriod(
                                        int.parse(value),
                                      ),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                      keyboardType: TextInputType.number,
                                      validator: (String value) {
                                        return (value != null)
                                            ? 'Alarm time period cannot be empty'
                                            : null;
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: kMarginNormal,
                                  ),
                                  CustomButton(
                                    label: "UPDATE",
                                    onPressed: () => settingsController
                                        .postDeviceSettings(1),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: kMarginSmall,
                    ),
                    CustomCard(
                      color: Colors.white,
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              decoration: kDecorationTopCard,
                              padding: EdgeInsets.all(kPaddingNormal),
                              child: Center(
                                child: Text(
                                  "Update Device Location",
                                  style: kTextStyleCardTitle,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(kPaddingNormal),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text("Select Location"),
                                  Obx(
                                    () => DropdownButton(
                                      key: Key("current_location"),
                                      elevation: kDropDownElevation,
                                      items: settingsController.locationNames
                                          .map<DropdownMenuItem<String>>(
                                        (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                      onChanged: (value) {
                                        settingsController
                                            .setCurrentLocation(value);
                                      },
                                      value: settingsController
                                          .selectedCurrentLocation.value,
                                      underline: Container(),
                                    ),
                                  ),
                                  SizedBox(
                                    height: kMarginNormal,
                                  ),
                                  Text("Select Device"),
                                  Obx(
                                    () => DropdownButton(
                                      elevation: kDropDownElevation,
                                      items: settingsController.locationDevices
                                          .map<DropdownMenuItem<String>>(
                                        (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                      onChanged: (value) {
                                        settingsController
                                            .selectedDevice(value);
                                      },
                                      value: settingsController
                                          .selectedDevice.value,
                                      underline: Container(),
                                    ),
                                  ),
                                  SizedBox(
                                    height: kMarginNormal,
                                  ),
                                  Text("Select New Location"),
                                  Obx(
                                    () => DropdownButton(
                                      key: Key("new_location"),
                                      elevation: kDropDownElevation,
                                      items: settingsController.locationNames
                                          .map<DropdownMenuItem<String>>(
                                        (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                      onChanged: (value) {
                                        settingsController
                                            .selectedNewLocation(value);
                                      },
                                      value: settingsController
                                          .selectedNewLocation.value,
                                      underline: Container(),
                                    ),
                                  ),
                                  SizedBox(
                                    height: kMarginNormal,
                                  ),
                                  CustomButton(
                                    label: "UPDATE",
                                    onPressed: () => settingsController
                                        .postDeviceSettings(4),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: kMarginNormal,
                    ),
                    // CustomButton(
                    //   color: Colors.red,
                    //   label: "LOG OUT",
                    //   onPressed: () => settingsController.logOut(),
                    // ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
