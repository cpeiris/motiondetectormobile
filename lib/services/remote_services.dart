import 'dart:convert';

import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:motion_detector/models/devices.dart';
import 'package:motion_detector/models/locationDevices.dart';
import 'package:motion_detector/models/locations.dart';
import 'package:motion_detector/models/login.dart';
import 'package:motion_detector/models/status.dart';

class RemoteServices {
  static var client = http.Client();

  static Future<List<Devices>> fetchDevices() async {
    print('User Id: ${GetStorage().read("user_id")}');

    var response = await client.post(
      'https://pv9iaxspm9.execute-api.ap-southeast-1.amazonaws.com/default/sADataManager',
      body: '{"type": "1", "user_id": "${GetStorage().read("user_id")}"}',
    );

    print('fetchDevices: ${response.body}');

    if (response.statusCode == 200) {
      var jsonString = response.body;
      return devicesFromJson(jsonString);
    } else {
      return null;
    }
  }

  static Future<List<Status>> fetchDeviceStatus(String devices) async {
    var response = await client.post(
      'https://pv9iaxspm9.execute-api.ap-southeast-1.amazonaws.com/default/sADataManager',
      body: '{"type": "2", "devices": "$devices"}',
    );

    print('fetchDeviceStatus: ${response.body}');

    if (response.statusCode == 200) {
      var jsonString = response.body;

      return statusFromJson(jsonString);
    } else {
      return null;
    }
  }

  static Future<bool> postDeviceCommand(String command, String deviceId) async {
    var response = await client.post(
      'https://9uvwm5gzg9.execute-api.ap-southeast-1.amazonaws.com/beta/DataCatcher',
      body: '{"message": "$command", "device_id": "$deviceId"}',
    );

    if (response.statusCode == 200)
      return true;
    else
      return false;
  }

  static Future<Login> login(String userName, String password) async {
    var response = await client.post(
      'https://pv9iaxspm9.execute-api.ap-southeast-1.amazonaws.com/default/sADataManager',
      body: '{"type": "5", "user_name": "$userName", "password": "$password"}',
    );

    if (response.statusCode == 200) {
      var jsonString = response.body;

      if (jsonString != "null")
        return loginFromJson(jsonString);
      else
        return null;
    } else {
      return null;
    }
  }

  static Future<List<Locations>> fetchLocations() async {
    var response = await client.post(
      'https://pv9iaxspm9.execute-api.ap-southeast-1.amazonaws.com/default/sADataManager',
      body: '{"type": "3", "user_id": "${GetStorage().read("user_id")}"}',
    );

    if (response.statusCode == 200) {
      var jsonString = response.body;
      return locationsFromJson(jsonString);
    } else
      return null;
  }

  static Future<List<LocationDevices>> fetchDevicesByLocation(
      String locationId) async {
    var response = await client.post(
      'https://pv9iaxspm9.execute-api.ap-southeast-1.amazonaws.com/default/sADataManager',
      body: '{"type": "4", "location_id": "$locationId"}',
    );

    if (response.statusCode == 200) {
      var jsonString = response.body;
      return locationDevicesFromJson(jsonString);
    } else
      return null;
  }

  static void postFirebaseToken({String firebaseToken}) async {
    var response = await client.post(
      'https://pv9iaxspm9.execute-api.ap-southeast-1.amazonaws.com/default/sADataManager',
      body:
          '{"type": "6", "user_id": "${GetStorage().read("user_id")}","firebase_token": "$firebaseToken"}',
    );

    print('firebase response: $response');
  }

  static Future<bool> postSettings(
      {int whichSettings,
      int locationId,
      String alarmPeriod,
      String deviceName,
      int deviceId}) async {
    String body;

    if (whichSettings == 1)
      body =
          '{"status": $whichSettings, "period": "$alarmPeriod", "location_id": $locationId}';
    else if (whichSettings == 4)
      body =
          '{"status": $whichSettings, "device_id": $deviceId, "device_name": "$deviceName", "location_id": $locationId}';

    var response = await client.post(
      'https://9uvwm5gzg9.execute-api.ap-southeast-1.amazonaws.com/beta/Configuration_App',
      body: body,
    );

    print(body);

    if (response.statusCode == 200) {
      var jsonString = response.body;
      Map<String, dynamic> jsonDecoded = jsonDecode(jsonString);

      try {
        if (jsonDecoded['success'])
          return true;
        else
          return false;
      } catch (e) {
        return false;
      }
    } else
      return false;
  }
}
