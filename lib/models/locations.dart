import 'dart:convert';

List<Locations> locationsFromJson(String str) =>
    List<Locations>.from(json.decode(str).map((x) => Locations.fromJson(x)));

String locationsToJson(List<Locations> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Locations {
  Locations({
    this.locationId,
    this.locationName,
    this.triggerPeriod,
  });

  int locationId;
  String locationName;
  int triggerPeriod;

  factory Locations.fromJson(Map<String, dynamic> json) => Locations(
        locationId: json["location_id"],
        locationName: json["location_name"],
        triggerPeriod: json["trigger_period"],
      );

  Map<String, dynamic> toJson() => {
        "location_id": locationId,
        "location_name": locationName,
        "trigger_period": triggerPeriod,
      };
}
