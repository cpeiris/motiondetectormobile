import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {
  Login({
    this.name,
    this.userId,
    this.userRoleId,
    this.password,
  });

  String name;
  int userId;
  int userRoleId;
  String password;

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        name: json["name"],
        userId: json["user_id"],
        userRoleId: json["user_role_id"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "user_id": userId,
        "user_role_id": userRoleId,
        "password": password,
      };
}
