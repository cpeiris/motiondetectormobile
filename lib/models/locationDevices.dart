import 'dart:convert';

List<LocationDevices> locationDevicesFromJson(String str) =>
    List<LocationDevices>.from(
        json.decode(str).map((x) => LocationDevices.fromJson(x)));

String locationDevicesToJson(List<LocationDevices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LocationDevices {
  LocationDevices({
    this.deviceId,
    this.deviceName,
  });

  int deviceId;
  String deviceName;

  factory LocationDevices.fromJson(Map<String, dynamic> json) =>
      LocationDevices(
        deviceId: json["device_id"],
        deviceName: json["device_name"],
      );

  Map<String, dynamic> toJson() => {
        "device_id": deviceId,
        "device_name": deviceName,
      };
}
