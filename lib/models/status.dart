import 'dart:convert';

List<Status> statusFromJson(String str) =>
    List<Status>.from(json.decode(str).map((x) => Status.fromJson(x)));

String statusToJson(List<Status> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Status {
  Status({
    this.autoId,
    this.deviceId,
    this.data,
  });

  int autoId;
  int deviceId;
  String data;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        autoId: json["auto_id"],
        deviceId: json["device_id"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "auto_id": autoId,
        "device_id": deviceId,
        "data": data,
      };
}
