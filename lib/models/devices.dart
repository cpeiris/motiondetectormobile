import 'dart:convert';

List<Devices> devicesFromJson(String str) =>
    List<Devices>.from(json.decode(str).map((x) => Devices.fromJson(x)));

String devicesToJson(List<Devices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Devices {
  Devices({
    this.deviceId,
    this.deviceName,
    this.locationName,
    this.status,
    this.isON,
    this.lastUpdated,
  });

  int deviceId;
  String deviceName;
  String locationName;
  String status;
  bool isON;
  String lastUpdated;

  factory Devices.fromJson(Map<String, dynamic> json) => Devices(
        deviceId: json["device_id"],
        deviceName: json["device_name"],
        locationName: json["location_name"],
        status: json["status"],
        isON: json["isON"],
        lastUpdated: json["last_updated"],
      );

  Map<String, dynamic> toJson() => {
        "device_id": deviceId,
        "device_name": deviceName,
        "location_name": locationName,
        "status": status,
        "isON": isON,
        "last_updated": lastUpdated,
      };
}
